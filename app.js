$(function(){

    heigth = 0;
    pantalla = $("#pantalla");
    tickets = [10, 20, 50, 100];

    $(".botones").on("click", ".btn-numeros", function(){
        var btn = $(this);
        $("#pantalla").val(function(){
            return pantalla.val() + btn.text();
        }).focus();
    });

    $(".btn-cancelar").on("click", function(){
       reset();
    });

    $(".btn-aceptar").on("click", function(e){
        var money = parseInt(pantalla.val());
        
        if(isNaN(money)){
            alert("Ingrese una cantidad por favor!");
            return false;
        }

        if(money >= 0 && money < 10 || !multiplo_10(money)){
            alert("Lo sentimos, no podemos entregarle tickets con esta denominación");
            return false;
        }

        var res = ticket_type(money);
        
        res.forEach(obj => {
            $("#resultado").append(`<li>${obj}</li>`);
        });

        dar_dinero();
        intert = setInterval(dar_dinero, 50);
    });

    // FUNCIONES
    dar_dinero = () => {
        $(".dinero").css("height", heigth + "px");
        if(heigth === 50) $(".centro").show();
        
        if(heigth === 100) {
            clearInterval(intert);
            alert("Tome su dinero");
            reset();
        }

        heigth++;
    };

    reset = () => {
        heigth = 0;
        $(".btn-aceptar").prop('disabled', false);
        $(".dinero").css("height", "0px");
        $("#pantalla").val(null);
        $("#resultado").empty();
        $(".centro").hide();
    }

    multiplo_10 = (cantidad) => {
        return ( cantidad % tickets[0] === 0 ) ? true : false;
    }

    ticket_type = (total) => {
        var resultado = [];
        if(total >= tickets[3]){ //100
            resultado.push(`${Math.floor( total / tickets[3] )} tickets de ${tickets[3]}`);
            total = total % tickets[3];
        }

        if(total >= tickets[2]){ //50
            resultado.push(`${Math.floor( total / tickets[2] )} tickets de ${tickets[2]}`);
            total = total % tickets[2];
        }
        
        if(total >= tickets[1]){ //20
            resultado.push(`${Math.floor( total / tickets[1] )} tickets de ${tickets[1]}`);
            total = total % tickets[1];
        }

        if(total >= tickets[0]){ //10
            resultado.push(`${Math.floor( total / tickets[0] )} tickets de ${tickets[0]}`);
            total = total % tickets[0];
        }

        return resultado;
    }

});