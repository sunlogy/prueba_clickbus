(function () {
    const botones = document.getElementsByClassName("btn-numeros");
    const btn_aceptar = document.getElementsByClassName("btn-aceptar")[0];
    const btn_cancelar = document.getElementsByClassName("btn-cancelar")[0];
    const dinero = document.getElementsByClassName("dinero")[0];
    const centro = document.getElementsByClassName("centro")[0];

    const pantalla = document.getElementById("pantalla");
    const resultado = document.getElementById("resultado");

    const tickets = [10, 20, 50, 100];
    let heigth = 0;

    Array.from(botones).forEach(btn => {
        btn.addEventListener('click', () => {
            pantalla.value = `${pantalla.value}${btn.textContent}`;
            pantalla.focus();
        });
    });

    btn_cancelar.addEventListener('click', () => {
        reset();
    });

    btn_aceptar.addEventListener('click', () => {
        var money = parseInt(pantalla.value);
        if (isNaN(money)) {
            alert("Ingrese una cantidad por favor!");
            return false;
        }

        if (money >= 0 && money < 10 || !multiplo_10(money)) {
            alert("Lo sentimos, no podemos entregarle tickets con esta denominación");
            return false;
        }

        var res = ticket_type(money);

        res.forEach(obj => {
            var node = document.createElement("LI");
            var textnode = document.createTextNode(obj);
            node.appendChild(textnode);
            resultado.appendChild(node);
        });

        dar_dinero();
        intert = setInterval(dar_dinero, 50);
    });


    // FUNCIONES
    multiplo_10 = (cantidad) => {
        return (cantidad % tickets[0] === 0) ? true : false;
    };

    dar_dinero = () => {
        dinero.style.height = `${heigth}px`;
        if (heigth === 50)
            centro.style.display = "block";

        if (heigth === 100) {
            clearInterval(intert);
            alert("Tome su dinero");
            reset();
        }

        heigth++;
    };

    ticket_type = (total) => {
        var resultado = [];
        if (total >= tickets[3]) { //100
            resultado.push(`${Math.floor(total / tickets[3])} tickets de ${tickets[3]}`);
            total = total % tickets[3];
        }

        if (total >= tickets[2]) { //50
            resultado.push(`${Math.floor(total / tickets[2])} tickets de ${tickets[2]}`);
            total = total % tickets[2];
        }

        if (total >= tickets[1]) { //20
            resultado.push(`${Math.floor(total / tickets[1])} tickets de ${tickets[1]}`);
            total = total % tickets[1];
        }

        if (total >= tickets[0]) { //10
            resultado.push(`${Math.floor(total / tickets[0])} tickets de ${tickets[0]}`);
            total = total % tickets[0];
        }

        return resultado;
    };

    reset = () => {
        heigth = 0;
        pantalla.value = "";
        resultado.innerHTML = "";
        dinero.style.height = "0px";
        centro.style.display = "none";
    };

})();